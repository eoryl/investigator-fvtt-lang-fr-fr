Hooks.once('init', () => {
    CONFIG.debug.hooks = true;


	if (typeof Babele !== 'undefined') {

		Babele.get().register({
			module: 'investigator_fr-FR',
			lang: 'fr',
			dir: 'compendium'
		});
	}
});