# Investigator FVTT-lang fr-FR

Localisation française pour le système Investigator (Gumshoe SRD compatible). Utilise le modèle Babele pour la traduction et des travaux du module de traduction "FoundryVTT dnd5e lang fr-FR. Merci a tous les contributeurs de ces différents système et modules.

Pour une installation manuelle copier coller dans le champs "Manifest URL" d'installation de modules.

```https://gitlab.com/eoryl/investigator-fvtt-lang-fr-fr/-/raw/main/investigator_fr-FR/module.json```

